//# sourceURL=d3ThreeWayPlausibility.js

'use strict';

window.smartRApp.directive('threewayplausibility', [
    'smartRUtils',
    'rServeService',
    function(smartRUtils, rServeService) {

        return {
            restrict: 'E',
            scope: {
                data: '=',
                width: '@',
                height: '@'
            },
            link: function (scope, element) {

                /**
                 * Watch data model (which is only changed by ajax calls when we want to (re)draw everything)
                 */
                scope.$watch('data', function() {
                    $(element[0]).empty();
                    if (! $.isEmptyObject(scope.data)) {
                        smartRUtils.prepareWindowSize(scope.width, scope.height);
                        createPlausibilityViz(scope, element[0]);
                    }
                });
            }
            };

       function createPlausibilityViz(scope, root) {
           var df = scope.data.input_data;
           var xArrLabel = scope.data.ont1;
           var yArrLabel = scope.data.ont2;
           //var zLabel = scope.data.ont3;
           var zArrLabel = scope.data.ont3;

           var margin = {top: 30, right: 170, bottom: 30, left: 120},
               width = 960 - margin.left - margin.right,
               height = 500 - margin.top - margin.bottom;
           var x = d3.scale.ordinal().rangeBands([0, width]),
               y = d3.scale.ordinal().rangeBands([height, 0]);
           var w = d3.scale.category10();

           var xStep;
           var yStep ;

               function make_x_axis() {
                                    return d3.svg.axis()
                                       .scale(x)
                                        .tickValues(d3.range(893, 1793, 100))
                                         .orient("bottom")
                                        }
               function make_y_axis() {
                                  return d3.svg.axis()
                                            .scale(y)
                                            .orient("left")
                                          }
                // plot one
                var svg = d3.select(root).append("svg")
                                        .attr("width", width + margin.left + margin.right)
                                        .attr("height", height + margin.top + margin.bottom)
                                        .append("g")
                                        .attr("transform", "translate(" + margin.left + "," + margin.top
                                                                         + ")");
                                 x.domain(df.map(function(d) { return d.var1; }));
                                 y.domain(df.map(function(d) { return d.var2; }));

                                 svg.selectAll(".tile")
                                            .data(df)
                                            .enter().append("rect")
                                            .attr("class", "tile")
                                            .attr("x", function(d) {
                                                 console.log(d.var1+"haha"+x(d.var1));
                                                 console.log(x.rangeBand(x.var1));
                                                    return x(d.var1); })
                                             .attr("y", function(d) {
                                                                             return y(d.var2); })
                                                 //.attr("width", width + margin.left + margin.right)
                                                 //.attr("height", height + margin.top + margin.bottom)
                                             .attr("width", x.rangeBand(x.var1))
                                             .attr("height",  y.rangeBand(y.var2))
                                             //.attr("height",  z.rangeBand(z.var4))
                                             .style("fill", function(d) { return w(d.plausibility_code); })
                                             .append("title")
                                             .text(function(d){return d.plausibility_code});
                 var legend = svg.selectAll(".legend")
                                                  .data(w.domain())
                                                  .enter().append("g")
                                                  .attr("class", "legend")
                                                  .attr("transform", function(d, i) { return "translate(" + (width +                                                                               20) + "," + (20 + i * 20) + ")"; });
                               legend.append("rect")
                                                  .attr("width", 20)
                                                  .attr("height", 20)
                                                  .style("fill", function (d) {return w(d);});
                               legend.append("text")
                                                  .attr("x", 26)
                                                  .attr("y", 10)
                                                  .attr("dy", ".35em")
                                                  .text(function(d) {return d;});
                               svg.append("text")
                               .attr("class", "label")
                                              .attr("x", width + 20)
                                              .attr("y", 10)
                                              .attr("dy", ".35em")
                                              .style("font-size", "14px")
                                              .style("text-decoration", "bold")
                                              .text("Plausibility_code");
                              svg.append("g")
                                            .attr("class", "x axis")
                                            .attr("transform", "translate(0," + height + ")")
                                            .call(d3.svg.axis().scale(x).orient("bottom"))
                                            .append("text")
                                            .attr("class", "label")
                                            .attr("x", width)
                                            .attr("y", -6)
                                            .attr("text-anchor", "end")
                                            .style("font-size", "14px")
                                            .style("text-decoration", "bold")
                                            .text(xArrLabel);
                               svg.append("g")
                                                .attr("class", "y axis")
                                                .call(d3.svg.axis().scale(y).orient("left"))
                                                .append("text")
                                                .attr("class", "label")
                                                .attr("y", 6)
                                                .attr("dy", ".71em")
                                                .attr("text-anchor", "end")
                                                .attr("transform", "rotate(-90)")
                                                .style("font-size", "14px")
                                                .style("text-decoration", "bold")
                                                .text(yArrLabel);
                               svg.append("text")
                                         .attr("x", (width / 2))
                                         .attr("y", 0 -(margin.top / 2))
                                         .style("font-size", "18px")
                                         .style("text-decoration", "bold")
                                         .attr("text-anchor", "middle")
                                         .text("Plausibility Check between Three Variables");

                // plot two
                var svg2 = d3.select(root).append("svg")
                                        .attr("width", width + margin.left + margin.right)
                                        .attr("height", height + margin.top + margin.bottom)
                                        .append("g")
                                        .attr("transform", "translate(" + margin.left + "," + margin.top
                                                                         + ")");
                                 x.domain(df.map(function(d) { return d.var1; }));
                                 y.domain(df.map(function(d) { return d.var3; }));

                                 svg2.selectAll(".tile")
                                            .data(df)
                                            .enter().append("rect")
                                            .attr("class", "tile")
                                            .attr("x", function(d) {
                                                 console.log(d.var1+"haha"+x(d.var1));
                                                 console.log(x.rangeBand(x.var1));
                                                    return x(d.var1); })
                                             .attr("y", function(d) {
                                                                             return y(d.var3); })
                                                 //.attr("width", width + margin.left + margin.right)
                                                 //.attr("height", height + margin.top + margin.bottom)
                                             .attr("width", x.rangeBand(x.var1))
                                             .attr("height",  y.rangeBand(y.var3))
                                             //.attr("height",  z.rangeBand(z.var4))
                                             .style("fill", function(d) { return w(d.plausibility_code); })
                                             .append("title")
                                             .text(function(d){return d.plausibility_code});
                 var legend2 = svg2.selectAll(".legend")
                                                  .data(w.domain())
                                                  .enter().append("g")
                                                  .attr("class", "legend")
                                                  .attr("transform", function(d, i) { return "translate(" + (width +                                                                               20) + "," + (20 + i * 20) + ")"; });
                               legend2.append("rect")
                                                  .attr("width", 20)
                                                  .attr("height", 20)
                                                  .style("fill", function (d) {return w(d);});
                               legend2.append("text")
                                                  .attr("x", 26)
                                                  .attr("y", 10)
                                                  .attr("dy", ".35em")
                                                  .text(function(d) {return d;});
                               svg2.append("text")
                                              .attr("class", "label")
                                              .attr("x", width + 20)
                                              .attr("y", 10)
                                              .attr("dy", ".35em")
                                              .style("font-size", "14px")
                                              .style("text-decoration", "bold")
                                              .text("Plausibility_code");
                              svg2.append("g")
                                            .attr("class", "x axis")
                                            .attr("transform", "translate(0," + height + ")")
                                            .call(d3.svg.axis().scale(x).orient("bottom"))
                                            .append("text")
                                            .attr("class", "label")
                                            .attr("x", width)
                                            .attr("y", -6)
                                            .attr("text-anchor", "end")
                                            .style("font-size", "14px")
                                            .style("text-decoration", "bold")
                                            .text(xArrLabel);
                               svg2.append("g")
                                                .attr("class", "y axis")
                                                .call(d3.svg.axis().scale(y).orient("left"))
                                                .append("text")
                                                .attr("class", "label")
                                                .attr("y", 6)
                                                .attr("dy", ".71em")
                                                .attr("text-anchor", "end")
                                                .attr("transform", "rotate(-90)")
                                                .style("font-size", "14px")
                                                .style("text-decoration", "bold")
                                                .text(zArrLabel);
                // plot three
                var svg3 = d3.select(root).append("svg")
                                        .attr("width", width + margin.left + margin.right)
                                        .attr("height", height + margin.top + margin.bottom)
                                        .append("g")
                                        .attr("transform", "translate(" + margin.left + "," + margin.top
                                                                         + ")");
                                 x.domain(df.map(function(d) { return d.var2; }));
                                 y.domain(df.map(function(d) { return d.var3; }));

                                 svg3.selectAll(".tile")
                                            .data(df)
                                            .enter().append("rect")
                                            .attr("class", "tile")
                                            .attr("x", function(d) {
                                                 console.log(d.var1+"haha"+x(d.var2));
                                                 console.log(x.rangeBand(x.var2));
                                                    return x(d.var2); })
                                             .attr("y", function(d) {
                                                                             return y(d.var3); })
                                                 //.attr("width", width + margin.left + margin.right)
                                                 //.attr("height", height + margin.top + margin.bottom)
                                             .attr("width", x.rangeBand(x.var2))
                                             .attr("height",  y.rangeBand(y.var3))
                                             //.attr("height",  z.rangeBand(z.var4))
                                             .style("fill", function(d) { return w(d.plausibility_code); })
                                             .append("title")
                                             .text(function(d){return d.plausibility_code});
                 var legend3 = svg3.selectAll(".legend")
                                                  .data(w.domain())
                                                  .enter().append("g")
                                                  .attr("class", "legend")
                                                  .attr("transform", function(d, i) { return "translate(" + (width +                                                                               20) + "," + (20 + i * 20) + ")"; });
                               legend3.append("rect")
                                                  .attr("width", 20)
                                                  .attr("height", 20)
                                                  .style("fill", function (d) {return w(d);});
                               legend3.append("text")
                                                  .attr("x", 26)
                                                  .attr("y", 10)
                                                  .attr("dy", ".35em")
                                                  .text(function(d) {return d;});
                               svg3.append("text")
                                              .attr("class", "label")
                                              .attr("x", width + 20)
                                              .attr("y", 10)
                                              .attr("dy", ".35em")
                                              .style("font-size", "14px")
                                              .style("text-decoration", "bold")
                                              .text("Plausibility_code");
                              svg3.append("g")
                                            .attr("class", "x axis")
                                            .attr("transform", "translate(0," + height + ")")
                                            .call(d3.svg.axis().scale(x).orient("bottom"))
                                            .append("text")
                                            .attr("class", "label")
                                            .attr("x", width)
                                            .attr("y", -6)
                                            .attr("text-anchor", "end")
                                            .style("font-size", "14px")
                                            .style("text-decoration", "bold")
                                            .text(yArrLabel);
                               svg3.append("g")
                                                .attr("class", "y axis")
                                                .call(d3.svg.axis().scale(y).orient("left"))
                                                .append("text")
                                                .attr("class", "label")
                                                .attr("y", 6)
                                                .attr("dy", ".71em")
                                                .attr("text-anchor", "end")
                                                .attr("transform", "rotate(-90)")
                                                .style("font-size", "14px")
                                                .style("text-decoration", "bold")
                                                .text(zArrLabel);


}
}
]);
