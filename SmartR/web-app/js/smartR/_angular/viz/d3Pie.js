//# sourceURL=d3Pie.js

'use strict';

window.smartRApp.directive('pie', [
    'smartRUtils',
    'rServeService',
    function(smartRUtils, rServeService) {

        return {
            restrict: 'E',
            scope: {
                data: '=',
                width: '@',
                height: '@'
            },
            link: function (scope, element) {

                /**
                 * Watch data model (which is only changed by ajax calls when we want to (re)draw everything)
                 */
                scope.$watch('data', function() {
                    $(element[0]).empty();
                    if (! $.isEmptyObject(scope.data)) {
                        smartRUtils.prepareWindowSize(scope.width, scope.height);
                        createPieViz(scope, element[0]);
                    }
                });
            }
            };

       function createPieViz(scope, root) {
           var df = scope.data.input_data;
           var ont = scope.data.ont;

      	   var margin = {top: 20, right: 90, bottom: 30, left: 120},
      		       width = 960 - margin.left - margin.right,
      		       height = 500 - margin.top - margin.bottom;
  	       var x = d3.scale.ordinal()
  	                       .rangeRoundBands([0, width], .1);

  	       var y = d3.scale.linear()
  	                       .range([height, 0]);
                 var xAxis = d3.svg.axis()
  		                 .scale(x)
  		                 .orient("bottom");
  	        var yAxis = d3.svg.axis()
  	                          .scale(y)
  	                          .orient("left");
    	       var svg = d3.select(root).append("svg")
    	                                .attr("width", width + margin.left + margin.right)
    	                                .attr("height", height + margin.top + margin.bottom)
    	                                .append("g")
    	                                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    	       		x.domain(df.map(function(d) { return d.category_value; }));
    	                y.domain([0, d3.max(df, function(d) { return d.frequency; })]);

    	       		svg.append("g")
    	                   .attr("class", "x axis")
    	                   .attr("transform", "translate(0," + height + ")")
    	                   .call(xAxis);

    	       		svg.append("g")
    	                   .attr("class", "y axis")
    	                   .call(yAxis)
    	                   .append("text")
    	                   .attr("transform", "rotate(-90)")
    	                   .attr("y", 6)
    	                   .attr("dy", ".71em")
    	                   .style("text-anchor", "end")
    	                   .text("Frequency");

    	       		svg.selectAll(".bar")
    	                   .data(df)
    	                   .enter().append("rect")
    	                   .attr("class", "bar")
    	                   .attr("x", function(d) { return x(d.category_value); })
    	                   .attr("width", x.rangeBand())
    	                   .attr("y", function(d) { return y(d.frequency); })
    	                   .attr("height", function(d) { return height - y(d.frequency); });

    	                    function type(d) {
    			              d.frequency = +d.frequency;
    			              return d;
    			              }
  }
  }
  ]);
