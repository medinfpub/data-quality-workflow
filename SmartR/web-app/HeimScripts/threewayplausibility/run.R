library("jsonlite")
##Input-Daten = variable 'loaded_variables'
main <- function() {
          #uebergebene Parameter zwischenspeichern
          tmp.params <- fetch_params

  #uebergebene Variablen zwischenspeichern und zu DataFrame konvertieren
  tmp.var1 <- unlist(loaded_variables, recursive = FALSE)
    tmp.var1 <- as.data.frame(tmp.var1)

    #Duplikat-Spalten entfernen
    tmp.delColumns <- grep("Row.Label", colnames(tmp.var1))
      tmp.delColumns <- tmp.delColumns[2:length(tmp.delColumns)]
      tmp.var2 <- tmp.var1[,-tmp.delColumns]

        #temporaere Dateien freigeben
        remove(tmp.var1, tmp.delColumns)

        number <- length(tmp.params$ontologyTerms)

          tmp.nameList = character(number)
          tmp.keyList = character(number)
            for (i in 1:number) {
                        tmp.subIndex <- gregexpr(pattern = "\\\\", tmp.params$ontologyTerms[[i]]$key)[[1]]
              tmp.subLength <- length(tmp.subIndex)

                  tmp.malus <- 0;
                  if (is.null(tmp.params$ontologyTerms[[i]]$metadata$unitValues$normalUnits)) {tmp.malus <- 1}

                  tmp.nameList[i] <- substr(tmp.params$ontologyTerms[[i]]$key, tmp.subIndex[tmp.subLength-1-tmp.malus]+1, tmp.subIndex[tmp.subLength-tmp.malus]-1)
                     tmp.keyList[i] <- substr(tmp.params$ontologyTerms[[i]]$key, tmp.subIndex[3], tmp.subIndex[tmp.subLength-1])
                       }
           tmp.nameList <- unique(tmp.nameList);
             tmp.keyList <- unique(tmp.keyList)

             ## Format to tmp.var2 design
             tmp.keyList <- gsub(" ", ".", tmp.keyList)
               tmp.keyList <- gsub("\\(", ".", tmp.keyList)
               tmp.keyList <- gsub("\\)", ".", tmp.keyList)
                 tmp.keyList <- gsub("-", ".", tmp.keyList)
                 tmp.keyList <- gsub("\\/", ".", tmp.keyList)
                   tmp.keyList <- gsub("\\\\", ".", tmp.keyList)

                   colnames(tmp.var2)[1] <- "internalId"


                       for (i in 1:length(tmp.keyList)) {
                                   tmp.currentCols <- grep(tmp.keyList[i], colnames(tmp.var2))
                       tmp.var2[,tmp.currentCols[1]] <- as.character(tmp.var2[,tmp.currentCols[1]])

                           if (length(tmp.currentCols) > 1) {
                                         for (idx in tmp.currentCols[2:length(tmp.currentCols)]) {
                                                         tmp.var2[,tmp.currentCols[1]] <- ifelse(is.na(tmp.var2[,tmp.currentCols[1]]) | tmp.var2[,tmp.currentCols[1]] == "", as.character(tmp.var2[,idx]), tmp.var2[,tmp.currentCols[1]])
                                                         }

                             tmp.var2 <- tmp.var2[,-tmp.currentCols[2:length(tmp.currentCols)]]
                                 }



                           colnames(tmp.var2)[tmp.currentCols[1]] <- gsub(" ", "", tmp.nameList[i])
                               }
                     output <- calculate_dataquality(tmp.var2)
                     output_json <- toJSON(output, na = "string")
                    # output_json
                     write(output_json, "/tmp/exportthree.json")
                     output_json
}
calculate_dataquality <- function(input_data){
# subset
       library(dplyr)
      input_data[input_data==""]<-NA
      splitted_input <- split_dataset(input_data = input_data)
      input_data_numeric <- splitted_input$input_data_numeric
      input_data_categorical <- splitted_input$input_data_categorical
      
      # axis labels
      ontologies <- colnames(input_data_categorical)
      ont1 <- ontologies[1]
      ont2 <- ontologies[2]
      ont3 <- ontologies[3]
      
      # data to plot 3 way plausibility check
      table_data <- threeway_variable(input_data_categorical)
      
      return_value <- list("input_data"=table_data,"ont"=ontologies,"ont1"=ont1,"ont2"=ont2, "ont3"=ont3)
       
      return(return_value)
}

#eigene Funktionen, tmp.var2 ist Haupt-Variable (input_data)
split_dataset <- function(input_data){
  ## Schritt 0.2: Aufteilen in numerische und kategorielle Daten
  isnumeric <- sapply(input_data, is.numeric)
  input_data_numeric <- input_data[,isnumeric]
  input_data_categorical <- input_data[,!isnumeric]
  rm(isnumeric) #entfernen d. temporaeren Variable
  
  return_value <- list("input_data_numeric"=input_data_numeric, "input_data_categorical"=input_data_categorical)
  
  return(return_value)
}


# three_way plausibility check
threeway_variable <- function(df) {

  # cross tabulate 3 variables
  crost <- as.data.frame(table(df[,1], df[,2], df[,3], useNA = "ifany",
                                              dnn = c(colnames(df)[1], colnames(df)[2], colnames(df)[3])))
  library(dplyr)
    # slice data by frequency
    df_slice <- crost %>%
                group_by(crost[,1]) %>%
                    filter(Freq > 0)
    
    urindf <- c("stateurinordinary", "stateurinbloody", "stateurinopaque")
    
    # slice overlapping features
    df_slice$new_col <- ((rowSums(df_slice[,urindf] == "nein", na.rm=T) == 2) * 1)

    df_max <- df_slice %>%
	        group_by(new_col) %>%
		      filter(new_col == 0)
    
    df_max$plausibility_code <- ifelse(rowSums(df_max[,urindf] == "nein", na.rm=T) > 2, "implausible", "plausible")
    
    names(df_max) <- c("var1", "var2", "var3", "Freq", "plausibility_code")
    
    return(df_max)
}
